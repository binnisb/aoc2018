import dataclasses as dc
from typing import List
with open("puzzle8_input.txt") as fh:
    lines = list(map(int,map(str.strip,fh.read().split())))

@dc.dataclass(init=False)
class Node:
    name: int
    childs_count: int
    metadata_count: int
    childs: list
    metadata: list
    total: int

    def __init__(self,tree,ind,sum_method=0):
        self.name=ind
        self.childs_count = tree[ind]
        self.metadata_count = tree[ind+1]
        self.childs = []
        self.metadata = []

        next_node = ind+2
        for i in range(self.childs_count):
            self.childs.append(Node(tree, next_node,sum_method))
            next_node = self.childs[-1].end
        self.end = next_node + self.metadata_count
        self.metadata = tree[next_node:next_node+self.metadata_count]
        if sum_method == 0:
            self.total = sum(self.metadata)+sum([n.total for n in self.childs])
        else:
            if self.childs_count > 0:
                self.total = sum([self.childs[m-1].total for m in self.metadata if m <=len(self.childs)])
            else:
                self.total = sum(self.metadata)
                

test = list(map(int,map(str.strip,"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2".split())))

#head = Node(lines,0)
#print(head.total)
head = Node(lines,0,1)
print(head.total)
#print(head)