from typing import List
from collections import namedtuple
import numpy as np

Shape = namedtuple("Shape","id col row width height")
Shapes = List[Shape]
Lines = List[str]

def process_line(line:str) -> Shape:


    idn,_,coord,shape = line.strip().split()

    col,row = coord.strip(":").split(",")
    width,height = shape.lower().split("x")
    return Shape(idn, int(col), int(row), int(width), int(height))

def process_input(lines: Lines) -> Shapes:
    return [process_line(line) for line in lines]


def cut_cloth(snippets: Shapes) -> np.array:
    cloth = np.zeros((1000,1000))
    for snippet in snippets:
        cloth[snippet.row:snippet.row+snippet.height, snippet.col:snippet.col+snippet.width] += 1
    return cloth

def calculate_overlap(cloth: np.array, allowed_overlaps: int) -> int:

    return (cloth > (allowed_overlaps + 1)).sum()

def puzzle3(lines: Lines) -> int:
    snippets = process_input(lines)
    cloth = cut_cloth(snippets)

    return calculate_overlap(cloth,0)
    
def puzzle3_1():
    with open("./puzzle3_input.txt") as fh:
        lines = fh.readlines()
    print(puzzle3(lines))

def puzzle3_2():
    with open("./puzzle3_input.txt") as fh:
        lines = fh.readlines()
    shapes = process_input(lines)
    cloth = cut_cloth(shapes)
    for shape in shapes:
        if np.max(cloth[shape.row:shape.row+shape.height, shape.col:shape.col+shape.width]) == 1:
            print(shape.id)

def puzzle_test():
    lines = ["#1 @ 1,3: 4x4","#2 @ 3,1: 4x4","#3 @ 5,5: 2x2"]
    print(puzzle3(lines))

#puzzle_test()
#puzzle3_1()
puzzle3_2()