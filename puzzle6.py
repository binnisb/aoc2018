import dataclasses as dc
import numpy as np
from collections import namedtuple, defaultdict

@dc.dataclass
class Point:
    name: int
    x: int
    y: int

@dc.dataclass
class GridPoint:
    dist: int
    name: int

def parse_input(line,i):
    x,y = line.split(",")
    x = int(x)
    y = int(y)
    return Point(i,x,y)

with open("puzzle6_input.txt") as fh:
    points = {i:parse_input(l,i) for i,l in enumerate(fh)}

max_x = max_y = 0
for k,p in points.items():
    max_x = max(max_x,p.x)
    max_y = max(max_y,p.y)

board = [[None]*(max_y+1) for i in range(max_y+1)]
#board = np.zeros((max_x+1,max_y+1))

for i in range(len(board)):
    for j in range(len(board[0])):
        dists = [(p.name,abs(i-p.x)+abs(j-p.y)) for _,p in points.items()]
        m=100000
        node=None
        for n,d in dists:
            if d < m:
                m=d
                node = n
            elif d == m:
                node = -1
        board[i][j] = GridPoint(dist=m,name=node)

counts = defaultdict(int)
for row in board:
    for c in row:
        counts[c.name] += 1
edge = set(p.name for p in board[0])
edge.update(set(p.name for p in board[-1]))
edge.update(set(board[i][0].name for i in range(len(board)) ))
edge.update(set(board[i][-1].name for i in range(len(board)) ))

print(max([counts[k] for k in (counts.keys()-edge)]))




count = 0
for i in range(len(board)):
    for j in range(len(board[0])):
        dists = [(abs(i-p.x)+abs(j-p.y)) for _,p in points.items()]
        if sum(dists) < 10000:
            count += 1
print(count)
        