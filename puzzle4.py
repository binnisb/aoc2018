import numpy as np
import typing
from  typing import List, Dict, NamedTuple, Set, Tuple
Lines = List[str]
Guards = Dict[int,np.array]

def input() -> Lines:
    with open("puzzle4_input.txt") as fh:
        return sorted(map(str.strip,fh.readlines())) 

def guard_shifts(lines: Lines) -> Tuple[int,np.array]:
    guard_id = int(lines[0].split("] ")[1].split()[1][1:])

    shift = np.zeros(60,dtype=int)

    for sleep,awake in zip(lines[1::2],lines[2::2]):
        sleep = int(sleep.split(":")[1][:2])
        awake = int(awake.split(":")[1][:2])
        shift[sleep:awake] = 1

    return guard_id, shift


def parse_lines(lines: Lines) -> Guards:
    guards: Guards = dict()
    shifts = []
    for line in lines:
        if "#" in line:
            shift =[]
            shifts.append(shift)
        shift.append(line)

    for shift in shifts:
        g,s = guard_shifts(shift)
        if g not in guards:
            guards[g] = s
        else:
            guards[g] += s
    return guards

def max_guard_minute(guards: Guards) -> Tuple[int,int]:
    (s,i) = max([(shift.sum(),i) for (i,shift) in guards.items()])
    return guards[i].argmax()*i

#print( max_guard_minute( parse_lines( input() ) ) )
            
def max_minute_guard(guards: Guards) -> Tuple[int,int]:
    tup = sorted(guards.items())
    shifts = np.array([v for k,v in tup])
    x,y =  np.unravel_index(np.argmax(shifts, axis=None), shifts.shape)
    return tup[x][0] * y


print( max_minute_guard( parse_lines( input() ) ) )