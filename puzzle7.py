import networkx

with open("puzzle7_input.txt") as fh:
    edges = [(l[1],l[7]) for l in map(str.split,map(str.strip,fh))]

G = networkx.DiGraph(edges)

avail = set()
res = []
while True and G.nodes:
    for node in G.nodes:
        if G.in_degree(node) == 0:
            avail.add(node)
    curr = sorted(avail)[0]
    G.remove_node(curr)
    avail.remove(curr)
    res.append(curr)
print("".join(res))


G = networkx.DiGraph(edges)
sec_dict = {l:(60+t) for l,t in zip(sorted(res),range(1,len(res)+1))}

WORKERS=5
sec = 0
avail = set()
nodes_being_worked = dict()
res = []
while True and G.nodes and sec < 10000:
    to_remove = []
    for node in nodes_being_worked.keys():
        nodes_being_worked[node]-=1
        if nodes_being_worked[node] == 0:
            to_remove.append(node)

    for node in sorted(to_remove):
        del nodes_being_worked[node]
        G.remove_node(node)
        res.append(node)

    for node in G.nodes:
        if node not in nodes_being_worked.keys() and G.in_degree(node) == 0:
            avail.add(node)
    nodes = sorted(avail)
    for node in nodes:
        if len(nodes_being_worked) < WORKERS:
            nodes_being_worked[node] = sec_dict[node]
            avail.remove(node)
    sec += 1

print("".join(res))
print(sec-1)
