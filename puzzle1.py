with open("./puzzle1_input.txt") as fh:
    nums = list(map(int,map(str.strip,fh.readlines())))
print(f"puzzle 1: {sum(nums)}")

seen = set([0])
iteration = 0
l = len(nums)
s = 0
while True and len(seen) < 1000000:
    s += nums[iteration % l]
    if s in seen:
        print("broke")
        break
    seen.add(s)
    iteration += 1
print(f"puzzle 2: {s}")