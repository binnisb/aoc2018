with open("puzzle5_input.txt") as fh:
    line = fh.readline().strip()

orig = line
print(len(line))

def get_pairs(line):
    chars = set(line)
    pairs = set()
    for char in chars:
        pairs.add(f"{char.lower()}{char.upper()}")
        pairs.add(f"{char.upper()}{char.lower()}")
    return pairs

def react(line,pairs):
    pairs=get_pairs(line)
    i = 1
    while True and i < len(line):
        if line[i-1:i+1] in pairs:
            line = line[:i-1]+line[i+1:]
            i = max(1,i-1)
        else:
            i+=1
    return line

pairs = get_pairs(line)
print(len(react(line,pairs)))

res = {}

for p in pairs:
    if p.lower() in res:
        continue
    line = orig
    for c in p:
        line = line.replace(c,"")
    line = react(line,pairs)
    res[p.lower()]=len(line)

print(min(res.items(),key=lambda x: x[1]))