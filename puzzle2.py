from collections import defaultdict, Counter
with open("./puzzle2_input.txt") as fh:
    ids = list(map(str.strip,fh.readlines()))

def count_chars(string: str) -> list:
    charcount = defaultdict(int)
    for char in string:
        charcount[char] += 1
    return charcount

def calc_checksum(ids: list) -> int:
    exact_2 = 0
    exact_3 = 0
    for id in ids:
        counts = count_chars(id)
        if 2 in counts.values():
            exact_2 += 1
        if 3 in counts.values():
            exact_3 += 1
    return exact_2*exact_3

print(calc_checksum(ids))

def compare_ids(id1:str,id2:str, threshold:int) -> bool:
    """Returns True if the difference between id1 and id2 is leq than threshold else False
    """
    count = 0
    for u,l in zip(id1,id2):
        if u != l:
            count += 1
            if count > threshold:
                return False
    return True
    
def find_common_string(s1:str, s2:str) -> str:
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            return f"{s1[:i]}{s1[i+1:]}"

def find_packages(ids: list) -> str:
    threshold=1
    for j in range(len(ids)):
        for i in range(j+1,len(ids)):
            if compare_ids(ids[i],ids[j],threshold):
                return find_common_string(ids[j],ids[i])
                
print(find_packages(ids))
